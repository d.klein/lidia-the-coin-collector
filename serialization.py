import json


# data -> message
def serialize(data):
    return json.dumps(data)


# message -> data
def deserialize(message):
    return json.loads(message)
