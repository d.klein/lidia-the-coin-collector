from serialization import serialize, deserialize
import sys
import zmq

# server.py <ip> <port>

ip = sys.argv[1]
port = int(sys.argv[2])

context = zmq.Context()
socket = context.socket(zmq.REP)


def run_server(ip, port):
    player_positions = {}

    while True:
        update = deserialize(socket.recv())

        player = update['player']
        new_position = update['position']

        player_positions[player] = new_position

        socket.send_string(serialize(player_positions))


socket.bind(f"tcp://{ip}:{port}")
run_server(ip, port)
