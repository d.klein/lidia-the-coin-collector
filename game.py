# minigame demo based on https://pygame-zero.readthedocs.io

# game.py <player> <server_ip> <server_port>
#
# game    ----  REQ ({'player': <name>, 'position': (<x>, <y>)})  ---->   server
#         <---  REP ({'<name>': <pos>, ...})


from serialization import serialize, deserialize
from pgzero.actor import Actor
from pgzero.keyboard import keyboard
import random
import pgzrun
import sys
import zmq

# window
WIDTH = 800
HEIGHT = 600

# player
lidia = Actor('lidia_south')
lidia.center = (WIDTH / 2, HEIGHT / 2)
lidia_stepsize = 4

# objective
coin = Actor('coin')
coin.center = (random.randrange(WIDTH), random.randrange(HEIGHT))

# colors
grass_color = (21, 137, 14)

context = zmq.Context()
socket = context.socket(zmq.REQ)

player_name = sys.argv[1]
TITLE = f"{player_name} the coin collector"
server_ip = sys.argv[2]
server_port = sys.argv[3]

player_positions = {}
players = {}


# called by game loop
# graphic output
def draw():
    screen.clear()
    screen.fill(grass_color)
    coin.draw()
    lidia.draw()
    if players:
        for player in players:
            if not player == player_name:
                players[player].draw()


# called by game loop
# control logic
def update():
    global player_positions

    if keyboard.left:
        lidia.image = 'lidia_west'
        lidia.x -= lidia_stepsize
    if keyboard.right:
        lidia.image = 'lidia_east'
        lidia.x += lidia_stepsize
    if keyboard.up:
        lidia.image = 'lidia_north'
        lidia.y -= lidia_stepsize
    if keyboard.down:
        lidia.image = 'lidia_south'
        lidia.y += lidia_stepsize
    if coin.collidepoint((lidia.x, lidia.y)):
        coin.center = (random.randrange(WIDTH), random.randrange(HEIGHT))

    data = {'player': player_name, 'position': (lidia.x, lidia.y)}
    try:
        socket.send_string(serialize(data))
    except zmq.error.ZMQError:
        pass

    try:
        player_positions = deserialize(socket.recv())
    except zmq.error.ZMQError:
        pass

    if player_positions:
        print(player_positions)
        for player in player_positions:
            if player in players:
                players[player].x = player_positions[player][0]
                players[player].y = player_positions[player][1]
            else:
                players[player] = Actor('lidia_south')


socket.connect(f"tcp://{server_ip}:{server_port}")
# start game loop
pgzrun.go()
